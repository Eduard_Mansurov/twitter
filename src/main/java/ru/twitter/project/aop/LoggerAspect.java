package ru.twitter.project.aop;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.twitter.project.model.Log;
import ru.twitter.project.service.LogService;


@Component
@Aspect
public class LoggerAspect {
    @Autowired
    LogService logService;

    @AfterReturning(pointcut = "@annotation(Loggable)",
            returning = "retVal")
    public void log(JoinPoint joinPoint, Object retVal) {
        Log newLog = new Log();
        newLog.setReturnObject(retVal.toString());
        newLog.setMethod(joinPoint.getSignature().getName());
        logService.log(newLog);
    }


}
