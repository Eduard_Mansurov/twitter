package ru.twitter.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.twitter.project.form.TwitForm;
import ru.twitter.project.model.User;
import ru.twitter.project.service.TwitService;
import ru.twitter.project.utils.AuthenticationUtil;

@Controller
public class HomePageController {

    @Autowired
    TwitService twitService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String homePage(ModelMap map) {
        User user = AuthenticationUtil.getAuthUser();
        if (user == null) {
            return "redirect:/login";
        }
        return "redirect:/page/" + user.getName();
    }
}
