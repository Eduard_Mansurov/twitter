package ru.twitter.project.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.twitter.project.model.User;
import ru.twitter.project.utils.AuthenticationUtil;

@Controller
public class LoginController {

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(
            @RequestParam(value = "error", required = false) String error,
            ModelMap map) {
        if (error != null) {
            map.addAttribute("error", "Invalid username and password!");
        }
        User user = AuthenticationUtil.getAuthUser();
        if (user != null)
            return "redirect:/page/" + user.getName();
        else
            return "login";
    }
}
