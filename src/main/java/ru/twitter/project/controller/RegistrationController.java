package ru.twitter.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.twitter.project.controller.validator.UserRegistrationValidator;
import ru.twitter.project.form.UserForm;
import ru.twitter.project.service.UserService;

@Controller
public class RegistrationController {

    @Autowired
    @Qualifier("UserService")
    UserService service;

    @Autowired
    private UserRegistrationValidator validator;

    @RequestMapping(value = "/registrationPage", method = RequestMethod.GET)
    public String renderRegPage(ModelMap map) {
        map.addAttribute("UserForm", new UserForm());
        return "registrationPage";
    }

    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public String addUser(@ModelAttribute("UserForm") UserForm userForm, @RequestParam("password") String password,
                          @RequestParam("confirmPassword") String confPass,
                          BindingResult bindingResult, ModelMap map) {
        validator.validate(userForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "registrationPage";
        } else if (service.findByName(userForm.getName()) != null) {
            return "registrationPage";
        } else {
            service.addUser(userForm);
            return "redirect:/";

        }
    }

}
