package ru.twitter.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.twitter.project.form.RetwitForm;
import ru.twitter.project.form.TwitForm;
import ru.twitter.project.model.Retwit;
import ru.twitter.project.model.Twit;
import ru.twitter.project.model.User;
import ru.twitter.project.service.RetwitService;
import ru.twitter.project.service.TwitService;
import ru.twitter.project.service.UserService;
import ru.twitter.project.utils.AuthenticationUtil;
import ru.twitter.project.utils.FormMapper;
import ru.twitter.project.utils.Helper;

import java.util.*;


@Controller
public class UserPageController {

    @Autowired
    UserService userService;
    @Autowired
    TwitService twitService;
    @Autowired
    RetwitService retwitService;

    @Transactional
    @RequestMapping(value = {"/page/{name}"}, method = RequestMethod.GET)
    public String homePageUser(@PathVariable("name") String name, ModelMap map) {
        User myUser = AuthenticationUtil.getAuthUser();
        Long id = null;
        if (myUser != null) {
            id = myUser.getId();
        }
        User user = userService.findByName(name);
        if (user == null) {
            return "redirect:/";
        }
        map.addAttribute("user", user);
        map.addAttribute("myUser", id);
        map.addAttribute("twit", new TwitForm());

        Collection<GrantedAuthority> authorityList = (Collection<GrantedAuthority>)
                SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        boolean isAdmin = false;
        for (GrantedAuthority authority : authorityList) {
            if (authority.getAuthority().equals("ROLE_ADMINISTRATOR"))
                isAdmin = true;
        }

        List<Twit> listTwit = twitService.findByUser(user);
        List<Retwit> listRetwit = retwitService.findByUser(user);

        List<Twit> twitsAccess = new ArrayList<>();
        List<Retwit> retwitsAccess = new ArrayList<>();



        if (listTwit.isEmpty() && listRetwit.isEmpty()) {
            return "/userPage";
        } else {
            for (Twit twit: listTwit) {
                if (isAdmin || (myUser == null && twit.getAccess().equals("open")) || (myUser !=null && (!twit.getAccess().equals("close")
                        && ((twit.getAccess().equals("open")
                        || myUser.getName().equals(name)
                        ||(twit.getAccessUsers() != null && twit.getAccessUsers().contains(myUser)))))))
                    twitsAccess.add(twit);
            }
            for (Retwit retwit: listRetwit) {
                if (isAdmin || (myUser == null && retwit.getAccess().equals("open")) || (myUser != null &&(!retwit.getAccess().equals("close")
                        && ((retwit.getAccess().equals("open")
                        || myUser.getName().equals(name)
                        ||(retwit.getAccessUsers() != null && retwit.getAccessUsers().contains(myUser)))))))
                    retwitsAccess.add(retwit);
            }

            map.addAttribute("list", Helper.sort(twitsAccess, retwitsAccess));
        }
        return "/userPage";
    }


    @RequestMapping(value = "/page/newTwit", method = RequestMethod.POST)
    public String newTwit(@ModelAttribute("twit") TwitForm twitForm, ModelMap map) {
        twitForm.setDateTwit(new Date());
        Twit twit = FormMapper.twitFormToTwit(twitForm, AuthenticationUtil.getAuthUser(), null);
        twitService.addTwit(twit);
        map.addAttribute("entry", twit);
        map.addAttribute("myUser", AuthenticationUtil.getAuthUser().getId());
        return "Twit";
    }

    @RequestMapping(value = "/deleteTwit", method = RequestMethod.POST)
    @ResponseBody
    public Boolean deleteTwit(@RequestParam("id") String id, ModelMap map) {
        for (Retwit items : retwitService.findByTwit(Long.parseLong(id))) {
            retwitService.deleteRetwit(items);
        }
        twitService.deleteTwit(Long.parseLong(id));
        return true;
    }

    @RequestMapping(value = "/deleteRetwit", method = RequestMethod.POST)
    @ResponseBody
    public Boolean deleteRetwit(@RequestParam("id") String id, ModelMap map) {
        retwitService.deleteRetwit(Long.parseLong(id));
        return true;
    }

    @RequestMapping(value = "/page/newRetwit", method = RequestMethod.POST)
    @ResponseBody
    public Boolean retwit(@ModelAttribute("retwit") RetwitForm retwitForm, ModelMap map) {
        retwitForm.setDateRetwit(new Date());
        retwitService.addRetwit(FormMapper.retwitFormToRetwit(retwitForm,
                twitService.findById(retwitForm.getTwitId()), AuthenticationUtil.getAuthUser(), null));
        return true;
    }

    @RequestMapping(value = "/page/editTwit", method = RequestMethod.POST)
    @ResponseBody
    public Boolean editTwit(@ModelAttribute("twit") TwitForm twitForm,
                            @RequestParam("date") String dateLong, ModelMap map) {
        Date date = new Date(Long.parseLong(dateLong));
        twitForm.setDateTwit(date);
        List<User> list = new ArrayList<User>();
        if (twitForm.getAccessUsers() != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(twitForm.getAccessUsers(), ", ");
            while (stringTokenizer.hasMoreTokens()) {
                User user = userService.findByName(stringTokenizer.nextToken());
                if (user != null) {
                    list.add(user);
                }
            }
        }
        Twit twit = FormMapper.twitFormToTwit(twitForm, AuthenticationUtil.getAuthUser(), list);
        twitService.editTwit(twit);
        return true;
    }

    @RequestMapping(value = "/changeTwitAccess", method = RequestMethod.POST)
    @ResponseBody
    public void changeTwitAccess(@RequestParam("id") String id, @RequestParam("access") String access) {
        Twit twit = twitService.findById(Long.parseLong(id));
        twit.setAccess(access);
        twitService.editTwit(twit);
    }

    @RequestMapping(value = "/changeRetwitAccess", method = RequestMethod.POST)
    @ResponseBody
    public void changeRetwitAccess(@RequestParam("id") String id, @RequestParam("access") String access) {
        Retwit retwit = retwitService.findById(Long.parseLong(id));
        retwit.setAccess(access);
        retwitService.editRetwit(retwit);
    }

    //for access
    @RequestMapping(value = "/accessEdit", method = RequestMethod.POST)
    @ResponseBody
    public void access(@RequestParam("type") String type, @RequestParam("id") String id, @RequestParam("list") String usersAccess) {
        List<User> list = new ArrayList<>();
        StringTokenizer stringTokenizer = new StringTokenizer(usersAccess, ", ");
        while (stringTokenizer.hasMoreTokens()) {
            User user = userService.findByName(stringTokenizer.nextToken());
            if (user != null) {
                list.add(user);
            }
        }
        if (type.equals("twit")) {
            Twit twit = twitService.findById(Long.parseLong(id));
            twit.setAccessUsers(list);
            twitService.editTwit(twit);
        } else {
            Retwit retwit= retwitService.findById(Long.parseLong(id));
            retwit.setAccessUsers(list);
            retwitService.editRetwit(retwit);
        }

    }
}
