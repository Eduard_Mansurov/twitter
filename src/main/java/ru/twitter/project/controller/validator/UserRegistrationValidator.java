package ru.twitter.project.controller.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.twitter.project.form.UserForm;
import ru.twitter.project.model.User;

@Component
public class UserRegistrationValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "registrationError.emptyNameField");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "registrationError.emptyPasswordField");

        UserForm userForm = (UserForm) target;
        if (!userForm.getPassword().equals(userForm.getConfirmPassword())
                && !userForm.getPassword().equals("") && !userForm.getConfirmPassword().equals("")) {
            errors.rejectValue("confirmPassword", "registrationError.confirmConfPassField");
        } else {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPassword",
                    "registrationError.emptyConfPassField");
        }
    }
}
