package ru.twitter.project.dao;

import ru.twitter.project.model.Retwit;
import ru.twitter.project.model.User;

import java.util.List;

public interface RetwitDao extends AbstractDao<Retwit, Long> {
    List<Retwit> findByUser(User user);

    List<Retwit> findByTwit(Long id);
}
