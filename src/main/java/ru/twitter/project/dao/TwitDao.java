package ru.twitter.project.dao;

import ru.twitter.project.model.Twit;
import ru.twitter.project.model.User;

import java.util.List;

public interface TwitDao extends AbstractDao<Twit, Long> {
    List<Twit> findByUser(User user);
}
