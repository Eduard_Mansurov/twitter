package ru.twitter.project.dao;

import ru.twitter.project.model.User;

public interface UserDao extends AbstractDao<User, Long> {

    User findByName(String name);
}
