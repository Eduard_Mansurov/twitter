package ru.twitter.project.dao.impl;

import org.springframework.stereotype.Repository;
import ru.twitter.project.dao.LogDao;
import ru.twitter.project.model.Log;

@Repository
public class LogDaoImpl extends AbstractDaoImpl<Log, Long> implements LogDao {
    protected LogDaoImpl() {
        super(Log.class);
    }
}
