package ru.twitter.project.dao.impl;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import ru.twitter.project.dao.TwitDao;
import ru.twitter.project.model.Twit;
import ru.twitter.project.model.User;

import java.util.List;

@Repository
public class TwitDaoImpl extends AbstractDaoImpl<Twit, Long> implements TwitDao {

    protected TwitDaoImpl() {
        super(Twit.class);
    }

    @Override
    public List<Twit> findByUser(User user) {
        return (List<Twit>) getCurrentSession().createCriteria(Twit.class)
                .add(Restrictions.eq("user", user))
                .addOrder(Order.desc("date")).list();
    }
}
