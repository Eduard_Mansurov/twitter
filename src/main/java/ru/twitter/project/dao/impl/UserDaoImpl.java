package ru.twitter.project.dao.impl;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import ru.twitter.project.dao.UserDao;
import ru.twitter.project.model.User;

@Repository
public class UserDaoImpl extends AbstractDaoImpl<User, Long> implements UserDao {

    protected UserDaoImpl() {
        super(User.class);
    }

    @Override
    public User findByName(String name) {
        return (User) getCurrentSession().createCriteria(User.class)
                .add(Restrictions.eq("name", name))
                .uniqueResult();
    }
}
