package ru.twitter.project.form;


import ru.twitter.project.model.User;

import java.util.Date;

public class RetwitForm {
    private Long id;
    private Long twitId;
    private boolean RetwitOrAnswer;
    private Date dateRetwit;
    private String answerText;
    private String access;
    private String accessUsers;

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public String getAccessUsers() {
        return accessUsers;
    }

    public void setAccessUsers(String accessUsers) {
        this.accessUsers = accessUsers;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public Date getDateRetwit() {
        return dateRetwit;
    }

    public boolean isRetwitOrAnswer() {
        return RetwitOrAnswer;
    }

    public void setRetwitOrAnswer(boolean retwitOrAnswer) {
        RetwitOrAnswer = retwitOrAnswer;
    }

    public void setDateRetwit(Date dateRetwit) {
        this.dateRetwit = dateRetwit;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTwitId() {
        return twitId;
    }

    public void setTwitId(Long twit) {
        this.twitId = twit;
    }
}
