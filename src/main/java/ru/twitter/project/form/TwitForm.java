package ru.twitter.project.form;

import java.util.Date;

public class TwitForm {
    private Long id;
    private Long userID;
    private Date dateTwit;
    private String location;
    private String twitText;
    private String access;
    private String accessUsers;

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public String getAccessUsers() {
        return accessUsers;
    }

    public void setAccessUsers(String accessUsers) {
        this.accessUsers = accessUsers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public Date getDateTwit() {
        return dateTwit;
    }

    public void setDateTwit(Date dateTwit) {
        this.dateTwit = dateTwit;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTwitText() {
        return twitText;
    }

    public void setTwitText(String twitText) {
        this.twitText = twitText;
    }
}
