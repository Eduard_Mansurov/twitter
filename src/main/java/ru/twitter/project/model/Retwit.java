package ru.twitter.project.model;

import javax.persistence.*;
import java.util.List;

@Entity(name = "retwit_table")
@Table(name = "retwit_table")
public class Retwit {

    @Id
    @SequenceGenerator(name = "RetwitIdSequence", sequenceName = "retwit_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RetwitIdSequence")
    @Column(name = "id")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @ManyToOne
    @JoinColumn(name = "twit")
    private Twit twit;
    @Column(name = "dateRetwit")
    private Long date;
    @Column(name = "retwitOrAnswer")
    private boolean flag;
    @Column(name = "answerText")
    private String answerText;
    @Column(name = "access")
    private String access;
    @ManyToMany
    @JoinTable(uniqueConstraints= {@UniqueConstraint(columnNames={"retwit_id","accessusers_id"})}, name = "accessRetwit",
            joinColumns =  {@JoinColumn(name = "retwit_id")}, inverseJoinColumns = @JoinColumn(name = "accessusers_id"))
    private List<User> accessUsers;

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Twit getTwit() {
        return twit;
    }

    public void setTwit(Twit twit) {
        this.twit = twit;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public List<User> getAccessUsers() {
        return accessUsers;
    }

    public void setAccessUsers(List<User> accessUsers) {
        this.accessUsers = accessUsers;
    }
}
