package ru.twitter.project.model;

import javax.persistence.*;
import java.util.List;

@Entity(name = "twit_table")
@Table(name = "twit_table")
public class Twit {

    @Id
    @SequenceGenerator(name = "TwitIdSequence", sequenceName = "twit_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TwitIdSequence")
    @Column(name = "id")
    private Long id;
    @Column(name = "twitText")
    private String text;
    @Column(name = "dateTwit")
    private Long date;
    @Column(name = "locationTwit")
    private String location;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @Column(name = "access")
    private String access;

    @ManyToMany
    @JoinTable(uniqueConstraints= {@UniqueConstraint(columnNames={"twit_id","accessusers_id"})}, name = "accessTwit",
            joinColumns =  {@JoinColumn(name = "twit_id")}, inverseJoinColumns = @JoinColumn(name = "accessusers_id"))
    private List<User> accessUsers;

    public List<User> getAccessUsers() {
        return accessUsers;
    }

    public void setAccessUsers(List<User> accessUsers) {
        this.accessUsers = accessUsers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }
}
