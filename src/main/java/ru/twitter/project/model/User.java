package ru.twitter.project.model;

import javax.persistence.*;

@Entity(name = "user_table")
@Table(name = "user_table")
public class User {

    @Id
    @SequenceGenerator(name = "UserIdSequence", sequenceName = "user_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UserIdSequence")
    @Column(name = "id")
    private Long id;
    @Column(name = "user_login")
    private String name;
    @Column(name = "user_password")
    private String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object obj) {
        User user = (User) obj;
        return (this.getId().equals(user.getId()) && this.getName().equals(user.getName())
                && this.getPassword().equals(user.getPassword()));
    }

}
