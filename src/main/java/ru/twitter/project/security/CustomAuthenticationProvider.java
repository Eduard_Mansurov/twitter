package ru.twitter.project.security;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.twitter.project.model.User;
import ru.twitter.project.service.UserService;
import ru.twitter.project.utils.MD5Generator;

import java.util.ArrayList;
import java.util.List;

public class CustomAuthenticationProvider implements AuthenticationProvider {

    UserService userService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();
        User user = userService.findByName(name);
        if (user == null)
            throw new UsernameNotFoundException("Incorrect login");
        if (MD5Generator.md5(password).equals(user.getPassword())) {
            System.out.println("Success");
            List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
            if (!user.getName().equals("admin")) grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
            else {
                grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMINISTRATOR"));
            }
            Authentication auth = new UsernamePasswordAuthenticationToken(user, null, grantedAuthorities);
            return auth;
        } else {
            throw new BadCredentialsException("Invalid password");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public UserService getUserService() {
        return userService;
    }
}
