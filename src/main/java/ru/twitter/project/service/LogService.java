package ru.twitter.project.service;

import ru.twitter.project.model.Log;

public interface LogService {
    Long log(Log log);
}
