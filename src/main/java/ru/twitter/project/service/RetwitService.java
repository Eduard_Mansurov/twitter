package ru.twitter.project.service;

import org.springframework.transaction.annotation.Transactional;
import ru.twitter.project.aop.Loggable;
import ru.twitter.project.model.Retwit;
import ru.twitter.project.model.User;

import java.util.List;

public interface RetwitService {
    Long addRetwit(Retwit retwit);

    void deleteRetwit(Long id);

    void deleteRetwit(Retwit entity);

    List<Retwit> findByTwit(Long id);

    List<Retwit> findByUser(User user);

    Retwit findById(Long id);

    @Transactional
    @Loggable
    void editRetwit(Retwit retwit);
}
