package ru.twitter.project.service;

import org.springframework.transaction.annotation.Transactional;
import ru.twitter.project.model.Retwit;
import ru.twitter.project.model.Twit;
import ru.twitter.project.model.User;

import java.util.List;

public interface TwitService {
    Long addTwit(Twit twit);

    void editTwit(Twit twit);

    void deleteTwit(Long id);

    List<Twit> findByUser(User user);

    Twit findById(Long id);
}
