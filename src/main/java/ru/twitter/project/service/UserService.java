package ru.twitter.project.service;

import ru.twitter.project.form.UserForm;
import ru.twitter.project.model.User;

import java.util.List;

public interface UserService {

     User findByName(String name);

     User addUser(UserForm userForm);

    List<User> findAll();
}
