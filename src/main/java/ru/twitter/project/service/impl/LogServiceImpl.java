package ru.twitter.project.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.twitter.project.dao.LogDao;
import ru.twitter.project.model.Log;
import ru.twitter.project.service.LogService;

@Service
public class LogServiceImpl implements LogService {

    @Autowired
    LogDao logDao;

    @Override
    @Transactional
    public Long log(Log log) {
        return logDao.save(log);
    }
}
