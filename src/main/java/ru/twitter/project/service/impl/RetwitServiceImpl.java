package ru.twitter.project.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.twitter.project.aop.Loggable;
import ru.twitter.project.dao.RetwitDao;
import ru.twitter.project.model.Retwit;
import ru.twitter.project.model.User;
import ru.twitter.project.service.RetwitService;

import java.util.List;

@Service
public class RetwitServiceImpl implements RetwitService {
    @Autowired
    RetwitDao retwitDao;

    @Override
    @Transactional
    @Loggable
    public Long addRetwit(Retwit retwit) {
        return retwitDao.save(retwit);
    }

    @Override
    @Transactional
    @Loggable
    public void deleteRetwit(Long id) {
        retwitDao.delete(id);
    }

    @Override
    @Transactional
    @Loggable
    public void deleteRetwit(Retwit entity) {
        retwitDao.delete(entity);
    }

    @Override
    @Transactional
    @Loggable
    public List<Retwit> findByTwit(Long id) {
        return retwitDao.findByTwit(id);
    }

    @Override
    @Transactional
    @Loggable
    public List<Retwit> findByUser(User user) {
        return retwitDao.findByUser(user);
    }

    @Override
    @Transactional
    @Loggable
    public Retwit findById(Long id) {
        return retwitDao.findById(id);
    }

    @Override
    @Transactional
    @Loggable
    public void editRetwit(Retwit retwit) {
        retwitDao.edit(retwit);
    }
}
