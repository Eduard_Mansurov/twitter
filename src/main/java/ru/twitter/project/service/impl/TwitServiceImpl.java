package ru.twitter.project.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.twitter.project.aop.Loggable;
import ru.twitter.project.dao.TwitDao;
import ru.twitter.project.model.Twit;
import ru.twitter.project.model.User;
import ru.twitter.project.service.TwitService;

import java.util.List;

@Service
public class TwitServiceImpl implements TwitService {
    @Autowired
    TwitDao twitDao;

    @Override
    @Transactional
    @Loggable
    public Long addTwit(Twit twit) {
        return twitDao.save(twit);
    }

    @Override
    @Transactional
    @Loggable
    public void editTwit(Twit twit) {
        twitDao.edit(twit);
    }


    @Override
    @Transactional
    @Loggable
    public void deleteTwit(Long id) {
        twitDao.delete(id);
    }

    @Override
    @Transactional
    @Loggable
    public List<Twit> findByUser(User user) {
        return twitDao.findByUser(user);
    }

    @Override
    @Transactional
    @Loggable
    public Twit findById(Long id) {
        return twitDao.findById(id);
    }
}
