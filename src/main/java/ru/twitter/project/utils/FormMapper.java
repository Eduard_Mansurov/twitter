package ru.twitter.project.utils;


import ru.twitter.project.form.RetwitForm;
import ru.twitter.project.form.TwitForm;
import ru.twitter.project.form.UserForm;
import ru.twitter.project.model.Retwit;
import ru.twitter.project.model.Twit;
import ru.twitter.project.model.User;

import java.util.Date;
import java.util.List;

public class FormMapper {

    // Converts User form to User model;
    public static User userFormToUser(UserForm userForm) {

        User user = new User();
        user.setId(userForm.getId());
        user.setName(userForm.getName());
        user.setPassword(userForm.getPassword());
        return user;

    }

    // Converts User model to User form;
    public static UserForm userToUserForm(User user) {

        UserForm userForm = new UserForm();
        userForm.setId(user.getId());
        userForm.setName(user.getName());
        userForm.setPassword(user.getPassword());
        return userForm;

    }

    public static TwitForm twitToTwitForm(Twit twit) {
        Date date = new Date(twit.getDate());
        TwitForm twitForm = new TwitForm();
        twitForm.setId(twit.getId());
        twitForm.setDateTwit(date);
        twitForm.setTwitText(twit.getText());
        twitForm.setLocation(twit.getLocation());
        twitForm.setUserID(twit.getUser().getId());
        String users = null;
        for (User user : twit.getAccessUsers()) {
            users += user.getName() + ",";
        }
        twitForm.setAccessUsers(users);
        twitForm.setAccess(twit.getAccess());
        return twitForm;
    }

    public static Twit twitFormToTwit(TwitForm twitForm, User user, List<User> list) {
        Twit twit = new Twit();
        if (twitForm.getId() != null) {
            twit.setId(twitForm.getId());
        }
        twit.setLocation(twitForm.getLocation());
        twit.setDate(twitForm.getDateTwit().getTime());
        twit.setText(twitForm.getTwitText());
        twit.setUser(user);
        twit.setAccess(twitForm.getAccess());
        twit.setAccessUsers(list);
        return twit;
    }

    public static Retwit retwitFormToRetwit(RetwitForm retwitForm, Twit twit, User user, List<User> list) {
        Retwit retwit = new Retwit();
        if (retwitForm.getId() != null) {
            retwit.setId(retwitForm.getId());
        }
        retwit.setDate(retwitForm.getDateRetwit().getTime());
        retwit.setTwit(twit);
        retwit.setUser(user);
        retwit.setFlag(retwitForm.isRetwitOrAnswer());
        retwit.setAnswerText(retwitForm.getAnswerText());

        retwit.setAccess(retwitForm.getAccess());
        retwit.setAccessUsers(list);
        return retwit;
    }

    public static RetwitForm retwitToRetwitForm(Retwit retwit) {
        Date date = new Date(retwit.getDate());
        RetwitForm retwitForm = new RetwitForm();
        retwitForm.setTwitId(retwit.getTwit().getId());
        retwit.setUser(retwit.getUser());
        retwitForm.setDateRetwit(date);
        String users = null;
        for (User user : retwit.getAccessUsers()) {
            users += user.getName() + ", ";
        }
        retwitForm.setAccessUsers(users);
        retwitForm.setAccess(retwit.getAccess());
        return retwitForm;
    }


}