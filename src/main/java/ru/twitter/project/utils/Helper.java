package ru.twitter.project.utils;

import ru.twitter.project.model.Retwit;
import ru.twitter.project.model.Twit;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class Helper {
    public static List<Object> sort(List<Twit> twits, List<Retwit> retwits) {

        ArrayList<Object> list = new ArrayList<>();

        ListIterator<Twit> twitIterator = twits.listIterator();
        ListIterator<Retwit> retwitIterator = retwits.listIterator();

        Twit twit = new Twit();
        Retwit retwit = new Retwit();


        if (!twits.isEmpty()) {
            twit = twitIterator.next();
        }
        if (!retwits.isEmpty()) {
            retwit = retwitIterator.next();
        }

        if (retwits.isEmpty() && !twits.isEmpty()) {
            list.addAll(twits);
            return list;
        } else if (twits.isEmpty() && !retwits.isEmpty()) {
            list.addAll(retwits);
            return list;
        } else {
            while (twitIterator.hasNext() || retwitIterator.hasNext()) {
                if (twit.getDate() > retwit.getDate()) {
                    if (twitIterator.hasNext()) {
                        list.add(twit);
                        twit = twitIterator.next();
                    } else {
                        list.add(twit);
                        while (retwitIterator.hasNext()) {
                            list.add(retwit);
                            retwit = retwitIterator.next();
                        }
                        list.add(retwit);
                    }
                } else {
                    if (retwitIterator.hasNext()) {
                        list.add(retwit);
                        retwit = retwitIterator.next();
                    } else {
                        list.add(retwit);
                        while (twitIterator.hasNext()) {
                            list.add(twit);
                            twit = twitIterator.next();
                        }
                        list.add(twit);
                    }
                }

            }
            if (twit.getDate() > retwit.getDate()) {
                if (!list.contains(twit))
                    list.add(twit);
                if (!list.contains(retwit)) {
                    list.add(retwit);
                }
            } else {
                if (!list.contains(retwit)) {
                    list.add(retwit);
                }
                if (!list.contains(twit))
                    list.add(twit);

            }
        }
        return list;
    }
}
