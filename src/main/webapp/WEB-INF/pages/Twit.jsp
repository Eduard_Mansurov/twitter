<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div class="twit${entry.id}">
    <strong>${entry.user.name}</strong>

    <div style="float: right" class="text-muted ${entry.id}">
        <c:if test="${(entry.location != null) && (entry.location != '')}">${entry.location},</c:if>
        <script type="text/javascript">
            $('.text-muted.' + ${entry.id}).append(moment(<c:out value="${entry.date}"/>).format('MMMM Do YYYY, h:mm:ss a'));
        </script>
    </div>
    <div>
        <h4><p>${entry.text}</p></h4>
    </div>
    <div class="row">
        <div class="icon-preview" style="float: right">
            <i class="mdi-action-delete deleteButton" id="${entry.id}"><span>delete</span></i>
        </div>
        <div class="icon-preview" style="float: right">
            <i class="mdi-editor-mode-edit editButton" id="${entry.id}"><span>edit</span></i>
        </div>
        <div class="icon-preview" style="float: right">
            <i class="mdi-content-reply replyButton"
               id="${entry.id}"><span>reply</span></i>
        </div>
        <div class="privateAccess icon-preview" style="float: right">
            <i class="mdi-social-people privateButton"
               data-toggle="modal" data-target="#access${entry.id}"
               twitid="${entry.id}"
               <c:if test="${entry.access != 'private'}">style="display: none"</c:if>><span>Edit access</span></i>
        </div>
        <div class="private"
             messageType="<c:out value="${entry.getClass().simpleName == 'Twit' ? 'twit': 'retwit'}"/>"
             twitId="${entry.id}" style="float: right">
            <select class="form-control select selectAccess" placeholder="Кому виден твит?">
                <option value="open">Открытый твит</option>
                <option value="private">Приватный твит</option>
                <option value="close">Закрытый от всех</option>
            </select>
            <script type="text/javascript">$(".select").dropdown({ "autoinit": ".select" });</script>
        </div>
        <div id="access${entry.id}" class="modal fade"
             tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal" aria-hidden="true">
                            ×
                        </button>
                        <h4 class="modal-title">Dialog</h4>
                    </div>
                    <div class="modal-body">
                        <%--<c:forEach var="user" items="${entry.accessUsers}">--%>

                        <%--</c:forEach>--%>

                        <textarea type="text" name="access"
                                       class="form-control floating-label newAccess${entry.id}"
                                       rows="3" placeholder="New access"
                                ></textarea>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary editAccess"
                                twitId="${entry.id}" data-dismiss="modal"
                                messageType="<c:out value="${entry.getClass().simpleName == 'Twit' ? 'twit': 'retwit'}"/>">
                            EDIT
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
