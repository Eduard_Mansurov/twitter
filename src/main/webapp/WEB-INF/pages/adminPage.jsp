<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title></title>
</head>
<body>
<c:forEach var="entry" items="${list}">
    <a href="/page/${entry.name}">${entry.name}</a><br/>
</c:forEach>
</body>
</html>
