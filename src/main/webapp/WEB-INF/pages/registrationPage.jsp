<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Registration page</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/roboto.min.css" rel="stylesheet">
    <link href="/resources/css/material-fullpalette.min.css" rel="stylesheet">
    <link href="/resources/css/ripples.min.css" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/noUiSlider/6.2.0/jquery.nouislider.min.js"></script>
    <script type="text/javascript" src="/resources/js/ripples.min.js"></script>
    <script type="text/javascript" src="/resources/js/material.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

    <style>
        .vertical-center {
            min-height: 100%; /* Fallback for browsers do NOT support vh unit */
            min-height: 100vh; /* These two lines are counted as one :-)       */

            display: flex;
            align-items: center;

        }
        .horizontal-center {
            margin-left: auto;
            margin-right: auto;
        }

    </style>
</head>
<body>
<script type="text/javascript">$(function () {
    $.material.init();
});</script>

<div class="container">
    <div class="row vertical-center">
        <div class="well bs-component horizontal-center col-lg-5">

            <form:form method="POST" action="/addUser" modelAttribute="UserForm">
                <fieldset>
                    <legend>Registration</legend>

                    <div class="col-lg-12">
                        <form:errors path="name"/><br>
                        <form:input type="text" class="form-control floating-label" path="name" maxlength="20"
                                    id="name"
                                    placeholder="Your name"/>
                    </div>

                    <div class="col-lg-12">
                        <form:errors path="password"/><br>
                        <form:input type="password" class="form-control floating-label" path="password"
                                    maxlength="10"
                                    id="password" placeholder="Password"/>
                    </div>

                    <div class="col-lg-12">
                        <form:errors path="confirmPassword"/><br>
                        <form:input type="password" class="form-control floating-label" path="confirmPassword"
                                    maxlength="10"
                                    id="confirmPassword" placeholder="Confirm password"/>
                    </div>

                    <div class="col-lg-11" style="margin-top: 10px">
                        <button name="submit" type="submit" value="submit" class="btn btn-primary">sign up</button>
                    </div>
                </fieldset>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>